/*jslint node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE PROPERTIES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var express = require("express");
var router = express.Router();
var pug = require("pug");
var path = require("path");
var cache = {};

// exports out the router
module.exports = router;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// redirects all requests to the www domain.
router.use(function ($request, $response, $next) {
    $response.render = function ($view, $data) {
        // create the options
        var filename = path.join(__dirname, "..", "views", $view);
        var options = {
            filename: filename,
            pretty: false,
            self: false,
            debug: false,
            compileDebug: false,
            cache: true
        };
        try {
            // check if we already have a cache
            if (!cache[$view]) {
                cache[$view] = pug.compileFile(filename, options);
            }
            // write the template to the response
            $response.write(cache[$view]($data));
            // end the response
            $response.end();
        } catch ($error) {
            $next($error);
        }
    };
    return $next();
});