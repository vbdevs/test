/*jslint node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// exports out the router
module.exports = function ($error, $request, $response, $next) {
    $response.status($error.status || 500);
    return $response.end();
};