/*jslint node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE PROPERTIES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var express = require("express");
var router = express.Router();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = router;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get("/", function ($request, $response) {
    
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// TODO: Query the datasource for all available products.
	// Optional: Add filtering and sorting options
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	$response.render("default.pug", {
		products: []
	});
});