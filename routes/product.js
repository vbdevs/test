/*jslint node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE PROPERTIES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = router;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ROUTES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get("/", bodyParser.urlencoded({
    extended: false
}));
router.get("/", function ($request, $response) {
    
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// TODO: Populate a blank for allowing the user to create a new
	// product.
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	$response.render("product/new.pug", {
		product: {}
	});
});

router.post("/", bodyParser.urlencoded({
    extended: false
}));
router.post("/", function ($request, $response) {
    
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// TODO: Save the changes from the request to the datasource
	// Optional: Add contraints such as field length, type, etc.
	// Optional: Send the user to the "edit" page of the product created
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	
	// return the user to the home page.
	$response.redirect("/");
});
 
router.get("/:id", function ($request, $response) {
    
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// TODO: Query the datasource for the specified product by id
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	$response.render("product/edit.pug", {
		product: {}
	});
});

router.delete("/:id", bodyParser.urlencoded({
    extended: false
}));
router.delete("/:id", function ($request, $response) {
    
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// TODO: Save the changes from the request to the datasource
	// Optional: Add contraints such as field length, type, etc.
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	$response.render("product/delete.pug", {
		product: {}
	});
});