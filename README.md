## VAST BRIDGES TEST

This repository is a bare bones application that is unfinished.  You will complete the project.

You will create a small application that lists a series a products, lets you create a product, and then lets you edit and ultimately delete the product.

## INSTRUCTIONS

1) Product model should consist of at least 4 fields Id, Name, Price, Description

2) Create a page listing the products in a table

3) Create the functionality to create a new product

4) Create the functionality to edit an existing product

5) Create the functionality to delete a product

## SUBMITTING RESULTS

1) Clone this repository `git clone [repository address]`

2) Make your changes

3) Create a new repository in either Bitbucket or Github and push your changes to your new repository

4) Email your new repository address (make sure it's public) to me at ivan.spaeth@vastbridges.com
	
