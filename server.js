/*jslint node: true */

"use strict";

var express = require("express");
var app = express();
var path = require("path");
var server = require("http").Server(app);
var favicon = require("serve-favicon");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * SETUP
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// setup pug
app.set('view engine', 'pug')
// enable strict routing
app.enable("strict routing");
// disable the server header strings
app.disable("x-powered-by");
app.disable("etag");
app.use(favicon(__dirname + "/public/favicon.ico"));

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPRESS MIDDLEWARE
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

app.use("/", express.static(path.join(__dirname, "public")));

// add the routes
app.use("/", require("./index.js"));


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * START SERVER
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// start the server
server.listen(8080, function () {
	console.log("Server started http://localhost:8080");
});